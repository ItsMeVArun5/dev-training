from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in dev_training/__init__.py
from dev_training import __version__ as version

setup(
	name="dev_training",
	version=version,
	description="frapp/erpnext training",
	author="varun",
	author_email="varunmewada1@gmail.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
