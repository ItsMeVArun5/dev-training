# Copyright (c) 2023, varun and contributors
# For license information, please see license.txt

import frappe
import json
from frappe.model.document import Document

class LeadDetails(Document):
	pass


# @frappe.whitelist()
# def insert_doc(doc):
# 	doc_dict = json.loads(doc) # convert JSON string into dictionary.

# 	# checking wheather lead detail already exist or not.
# 	exist = frappe.db.exists(
# 		{
# 			'doctype': 'Lead Details',
# 			'lead': doc_dict["lead"]
# 		}
# 	)

# 	if exist:
# 		# update values in existing document or table.
# 		frappe.db.set_value('Lead Details', exist, {
# 			'first_name': doc_dict["first_name"],
# 			'middle_name': doc_dict["middle_name"],
# 			'last_name': doc_dict["last_name"],
# 			'gender': doc_dict["gender"],
# 		})

# 		frappe.db.commit()

# 	else:		
# 		new_doc = frappe.get_doc({
# 			'doctype': 'Lead Details',
# 			'first_name': doc_dict["first_name"],
# 			'middle_name': doc_dict["middle_name"],
# 			'last_name': doc_dict["last_name"],
# 			'gender': doc_dict["gender"],
# 			'lead': doc_dict["lead"]
# 		})

# 		new_doc.insert()
# 		frappe.db.commit()
# 		# return 