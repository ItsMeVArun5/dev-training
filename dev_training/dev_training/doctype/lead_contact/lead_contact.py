# Copyright (c) 2023, varun and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class LeadContact(Document):
	pass

@frappe.whitelist()
def get_contact_details(name):
	doc = frappe.get_doc('Lead Contact', name)
	return doc