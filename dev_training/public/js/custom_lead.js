frappe.ui.form.on("Lead", {
   validate: function (frm) {
      let pan_number = frm.doc.custom_pan_number;

      // Regex for PAN validation.
      let pan_regex = /^[A-Z]{5}[0-9]{4}[A-Z]$/;

      if (!pan_regex.test(pan_number)) {
         frappe.throw("invalid PAN number");
      }
   },

   // after_save: function (frm) {
   //    let doc = {
   //       first_name: frm.doc.first_name,
   //       middle_name: frm.doc.middle_name,
   //       last_name: frm.doc.last_name,
   //       gender: frm.doc.gender,
   //       lead: frm.docname,
   //    };

   //    frappe.call({
   //       method:
   //          "dev_training.dev_training.doctype.lead_details.lead_details.insert_doc",

   //       args: {
   //          doc: doc,
   //       },
   //       freeze: true,
   //       async: false,
   //       freeze_message: __("Loading..."),

   //       // callback: function (res) {},
   //    });
   // },

   custom_contact: function (frm) {
      let lead_contact_doc_name = frm.doc.custom_contact;

      frappe.call({
         method:
            "dev_training.dev_training.doctype.lead_contact.lead_contact.get_contact_details",
         args: {
            name: lead_contact_doc_name,
         },
         freeze: true,
         freeze_message: __("Loading..."),

         callback: function (res) {
            frm.set_value({
               email_id: res.message.email,
               mobile_no: res.message.mobile_no,
               phone: res.message.phone,
               whatsapp_no: res.message.whatsapp,
               website: res.message.website,
            });
         },
      });
   },

   refresh: function (frm) {
      frm.add_custom_button(
         __("Hello"),
         () => frappe.msgprint("Hello World!!"),
         __("Action")
      );
      frm.add_custom_button(
         __("Fetch Name"),
         () => {
            frappe.prompt(
               {
                  label: "First Name",
                  fieldname: "first_name",
                  fieldtype: "Data",
               },
               (res) => {
                  frm.set_value({
                     first_name: res.first_name,
                  });
               }
            );
         },
         __("Action")
      );
   },
});
