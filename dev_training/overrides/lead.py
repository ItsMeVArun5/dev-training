from erpnext.crm.doctype.lead.lead import Lead
import frappe

class CustomLead(Lead):
   def after_insert(self):
      super().after_insert()

      doc = frappe.new_doc('Lead Details')

      doc.first_name = self.first_name
      doc.middle_name = self.middle_name
      doc.last_name = self.last_name
      doc.gender = self.gender
      doc.lead = self.name

      doc.insert()


   def on_update(self):
      super().on_update()
      lead_detail_doc = frappe.get_doc('Lead Details', {
         'lead': self.name
      })

      lead_detail_doc.first_name = self.first_name
      lead_detail_doc.middle_name = self.middle_name
      lead_detail_doc.last_name = self.last_name
      lead_detail_doc.gender = self.gender

      lead_detail_doc.save()